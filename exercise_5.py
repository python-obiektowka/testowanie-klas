from emp import Employee
import unittest

class TestEmployee(unittest.TestCase):
    def test_mail(self):
        a = Employee("John", "Smith", 40, 80000)
        self.assertEqual(a.email, 'john.smith@mail.com')

if __name__ == '__main__':
    unittest.main()