from tax import SimpleTaxCalculator
import unittest

class TestIncomeTax(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.calc = SimpleTaxCalculator()

    def test_income_below_threshold(self):
        expected  = 10200
        result = self.calc.income_tax(60000)
        self.assertEqual(expected, result)

    def test_income_equal_threshold(self):
        expected = 14539.76
        result = self.calc.income_tax(85528)
        self.assertEqual(expected, result)

    def test_income_threshold(self):
        expected = 19170.8
        result = self.calc.income_tax(100000)
        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()