from tax import SimpleTaxCalculator
import unittest

def setUpModule():
    global calc
    calc = SimpleTaxCalculator()

class TestIncomeTax(unittest.TestCase):
    def test_income_below_threshold(self):
        expected  = 10200
        result = calc.income_tax(60000)
        self.assertEqual(expected, result)

    def test_income_equal_threshold(self):
        expected = 14539.76
        result = calc.income_tax(85528)
        self.assertEqual(expected, result)

    def test_income_threshold(self):
        expected = 19170.8
        result = calc.income_tax(100000)
        self.assertEqual(expected, result)


class TestVatTax(unittest.TestCase):
    def test_vat_tax_with_default(self):
        expected  = 23.0
        result = calc.vat_tax(100)
        self.assertEqual(expected, result)

    def test_vat_tax_with_twenty_tax_rate(self):
        expected = 20.0
        result = calc.vat_tax(100,20.0)
        self.assertEqual(expected, result)


class TestCapitalGains(unittest.TestCase):
    def test_positive_profit(self):
        self.assertEqual(190.0, calc.capital_gains_tax(1000))

    def test_zero_profit(self):
        self.assertEqual(0, calc.capital_gains_tax(0))

    def test_negative_profit(self):
        self.assertEqual(0, calc.capital_gains_tax(-1000))

if __name__ == '__main__':
    unittest.main()