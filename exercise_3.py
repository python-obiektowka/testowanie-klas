from tax import SimpleTaxCalculator
import unittest

class TestCapitalGains(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.calc = SimpleTaxCalculator()

    def test_positive_profit(self):
        self.assertEqual(190.0, self.calc.capital_gains_tax(1000))

    def test_zero_profit(self):
        self.assertEqual(0, self.calc.capital_gains_tax(0))

    def test_negative_profit(self):
        self.assertEqual(0, self.calc.capital_gains_tax(-1000))

if __name__ == '__main__':
    unittest.main()