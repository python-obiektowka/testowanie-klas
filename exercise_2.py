from tax import SimpleTaxCalculator
import unittest

class TestVatTax(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.calc = SimpleTaxCalculator()

    def test_vat_tax_with_default(self):
        expected  = 23.0
        result = self.calc.vat_tax(100)
        self.assertEqual(expected, result)

    def test_vat_tax_with_twenty_tax_rate(self):
        expected = 20.0
        result = self.calc.vat_tax(100,20.0)
        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()