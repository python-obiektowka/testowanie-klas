from emp import Employee
import unittest

class TestEmployee(unittest.TestCase):
    def test_email_after_changing_first_name(self):
        a = Employee("John", "Smith", 40, 80000)
        a.first_name='Mike'
        self.assertEqual(a.email, "mike.smith@mail.com")


if __name__ == '__main__':
    unittest.main()